from flask import Flask, redirect, url_for, render_template, request,flash
from flaskext.mysql import MySQL


#from flask import flash


app = Flask(__name__)

# conexión a la base de datos mysql
mysql = MySQL()
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '1JMt05m'
app.config['MYSQL_DATABASE_DB'] = 'contactos'
mysql.init_app(app)


# setting
app.secret_key = 'mysecretkey'

@app.route('/', methods=['GET', 'POST'])
def home():
    sql= "SELECT * FROM contacts;"

    conn = mysql.connect()
    cursor= conn.cursor()
    cursor.execute(sql)

    contactos = cursor.fetchall()
    conn.commit()


    if request.method == 'POST':
        # Handle POST Request here
        return render_template('contactos/index.html')
    return render_template('contactos/index.html', conta=contactos)


@app.route('/add_contacts', methods=['POST'])
def add_contacts():
    if request.method == 'POST':
        nombre = request.form['nombre']
        apellido = request.form['apellido']
        direccion = request.form['direccion']
        cp = request.form['cp']
        provincia = request.form['provincia']
        comunidad = request.form['comunidad']

        sql = ("INSERT INTO contacts (nombre,apellido,direccion,cp,provincia,comunidad) VALUES(%s,%s,%s,%s,%s,%s);")

        datos =(nombre, apellido, direccion, cp, provincia, comunidad)

        # se ejecuta la instrucción
        conn=mysql.connect()
        cursor=conn.cursor()

        cursor.execute(sql, datos)
        conn.commit()
        flash('Contacto agregado exitosamente!')

    return redirect(url_for('home'))


@ app.route('/eliminar/<int:id>')
def eliminarContact(id):

    conn = mysql.connect()
    cursor = conn.cursor()

    cursor.execute("DELETE FROM contacts WHERE id=%s",(id))
    conn.commit()
    flash('Contacto eliminado!')
    return redirect('/')

@ app.route('/editar/<int:id>')
def editarContact(id):

    conn = mysql.connect()
    cursor = conn.cursor()

    cursor.execute("SELECT * FROM contacts WHERE  id = %s",(id))

    data = cursor.fetchall()
    conn.commit()

    return render_template('contactos/editar.html', editCont= data[0])

@app.route('/update/<id>', methods=['POST'])
def update(id):

    if request.method == 'POST':
        nombre = request.form['nombre']
        apellido = request.form['apellido']
        direccion = request.form['direccion']
        cp = request.form['cp']
        provincia = request.form['provincia']
        comunidad = request.form['comunidad']
        

        sql = "UPDATE contacts SET nombre=%s,apellido=%s,direccion=%s,cp=%s,provincia=%s,comunidad=%s WHERE id=%s"

        datos = (nombre,apellido,direccion,cp,provincia,comunidad,id)

        conn=mysql.connect()
        cursor=conn.cursor()

        cursor.execute(sql,datos)
        conn.commit()
        flash('Contacto modificado exitosamente!')

    return redirect('/')

    


if __name__ == '__main__':  # este codigo es para iniciar un servidor
    # DEBUG is SET to TRUE. CHANGE FOR PROD
    app.run(port=5000, debug=True)
